{{ config(materialized='table') }}

select date, 
    sum(new_confirmed) as sum_confirmed,
    sum(new_deceased) as sum_deceased,
    sum(new_recovered) as sum_recovered,
    sum(new_tested) as sum_tested
from google_covid_dataset 
group by date
order by date